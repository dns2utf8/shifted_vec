# shifted_vec

[![crates.io](https://img.shields.io/crates/v/shifted_vec?logo=rust)](https://crates.io/crates/shifted_vec/)
[![docs.rs](https://docs.rs/shifted_vec/badge.svg)](https://docs.rs/shifted_vec)
[![CI pipeline](https://gitlab.com/dns2utf8/shifted_vec/badges/master/pipeline.svg)](https://gitlab.com/dns2utf8/shifted_vec/)

A growable datastructure with positive and negative indexing built on top of `std::vec::Vec` calculating the offset automatically.


```
use shifted_vec::ShiftedVec;

let mut v = ShiftedVec::with_offset_and_capacity(-2, 5);

// populate the ShiftedVec
v.push(0);
v.push(1);
v.push(2);
v.push(3);
v.push(4);

assert_eq!(5, v.len());

assert_eq!(2, v[0]);

// mutable access with index
v[0] = 5;

assert_eq!(5, v[0]);
```

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.
