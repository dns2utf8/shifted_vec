# Changelog

## 0.1.2

* Add badges

## 0.1.1

* Remove no-std category from Cargo.toml

## 0.1.0

* Initial release
* Add CI
